#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "project.h"

/*
 * This is our main function for proccessing the tape and computing the multiplication.
 * We have an enum which is used to set our various states ie Q0 ... Q9
 * Q7 is our end state.
 *
 * NOTE: I have kept redundant writes in some places , i done this for clarity of the program.
 *
*/
void machine ( SYMBOL * tape  , size_t tape_length ){

	//seto ur inital position and state
	STATE state = Q0;
	int pos = 0;

	//loop while we are not in end state
	while ( state != Q10 ){

		//switch case our current state
		switch ( state ){

			case Q0:

					//if we read a one then write undefined and move right , set state to Q1
					if ( tape[ pos ] == S1 ){
						tape[ pos ] = S0;
						pos ++;
						state = Q1;

					//otherwise if we read undefined then , write undefined set state to Q7
					}else if ( tape[pos] == S0 ){
						pos --;
						state = Q7;
					}

				break;

			case Q1:

					//while we read a one move right
					if ( tape[pos] == S1  ){
						pos++;
					}

					//if we read undefined , write undefined move right and set state to Q2
					if ( tape[pos] == S0 ){
						pos++;
						state = Q2;
					}

				break;

			case Q2:

					//if we read a one , write an undefined, move right and set our state to Q3
					if ( tape[pos] == S1 ){
						tape[pos] = S0;
						pos++;
						state = Q3;

					//otherwise if we read an undefined , write a undefined , go left and set our state to Q8
					}else if ( tape[pos] == S0 ){
						state = Q8;
						pos--;
					}

				break;

			case Q3:

					//while we read ones move right
					if( tape[pos] == S1 ){
						pos++;
					}

					//if we read undefined , write undefined , move right and set state to Q4
					if ( tape[pos] == S0 ){
						state = Q4;
						pos++;
					}

				break;
			case Q4:

					// /while we are reading ones move right
					if ( tape[pos] == S1 ){
						pos++;
					}

					//if we read undefined , write a one , move left and set state to Q5
					if ( tape[pos] == S0 ){
						tape[pos] = S1;
						state = Q5;
						pos--;
					}

				break;

			case Q5:

					//while we are reading ones , move left
					if ( tape[pos] == S1 ){
						pos--;
					}

					//if we read a undefined , then write undefined , move left and set our state Q6
					if ( tape[pos] == S0 ){
						state = Q6;
						pos--;
					}

				break;

			case Q6:

					//while we are reading ones , move left
					if ( tape[pos] == S1 )
						pos--;

					//if we read an undefined , write a one move right and set our state to Q2
					if ( tape[pos] == S0 ){
						tape[pos] = S1;
						state = Q2;
						pos++;
					}

				break;

			case Q7: 

				if ( tape[pos] == S1 && pos > 0 ){
					pos --;
				}

				if ( pos <= 0 ){
					
					pos=0;
					state = Q10;
				}

				break;

			case Q8:

					//while we are reading ones , move left
					if ( tape[pos] == S1 )
						pos--;

					//if we read undefined , write undefined , move left and set state to Q9
					if ( tape[pos] == S0 ){
						state = Q9;
						pos--;
					}
				break;

			case Q9:

					//while we read ones , move left
					if ( tape[pos] == S1 )
						pos--;

					//if we read an undefined , write a ones , move right and set state to Q0
					if ( tape[pos] == S0 ){
						tape[pos] = S1;
						state = Q0;
						pos ++;
					}

				break;
		}

		//print the current tape
		printTape( tape , pos , tape_length );
	}
}


//this function is used to write the two numbers to the tape
void addToTape ( SYMBOL * tape , int nOne , int nTwo ){
	
	int i,j = 0;

	//we still need to add a zero character for our first digit
	if ( nOne == 0 )
		i=1;

	//here we are writing first batch of ones
	for ( ; i < nOne ; i ++ ){
		tape[i] = S1;
	}

	i++;

	//now write second batch
	for ( j = 0 ; j < nTwo ; j++ ){
		tape[ i+j ] = S1;
	}
}

//this function is used to count the result
int countResult ( SYMBOL * tape , size_t length , unsigned int nOne , unsigned int nTwo ){
	unsigned int count = 0;
	int i = 0;

	for( i = 0 ; i < length ; i++ ){
		if ( tape[i] == S1 )
			count++;
	}

	//return the count minue the two input numbers
	return (count-nOne-nTwo);
}

//this function is used to print our cursor location
void printTape ( SYMBOL * tape , unsigned int pos , size_t length ){

	unsigned int i = 0;

	printf("[");

	//print each character in our tape
	for ( i = 0 ; i < length ; i ++ ){

		if ( i == pos ){
			//print the current tape head position , with colours and fancy brackets to show our head position
			printf( "%s[%s]%s" , "\033[1;32m\033[1;34m" , ((tape[i] == S0) ? SYMB_0 : SYMB_1)  , "\033[0m" );
		}else{
			//print a normal tape symbol
			printf( ((tape[i] == S0) ? SYMB_0 : SYMB_1 ) );
		}

	}

	printf("]\n");
}