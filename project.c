#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "project.h"

//this is our tape
SYMBOL * tape;
size_t tape_length = 0;

int add(int first, int second)
{
    return first + second;
}

int main ( int argc , char ** args )
{
	//make sure we have enough arguments
	if ( argc != 3 ){
		fprintf( stderr , "Error: Incorrect arguments supplied.\n" );
		exit(1);
	}

	//now we have our arguments convert them to numbers
	int nOne = atoi( args[1] );
	int nTwo = atoi( args[2] );

	//now lets malloc enough room for our tape based on the numbers
	tape_length = ( (nOne*nTwo) + nOne + nTwo + 2 );
	tape = (SYMBOL * )calloc(  tape_length , sizeof(SYMBOL) );

	//init tape to all undefs
	int i =0;
	for ( i = 0 ; i < tape_length ; i ++ )
		tape[i] = S0;

	//add our numbers to the tape ( defined in machine.c )
	addToTape ( tape , nOne , nTwo );

	//print the clean tape :)
	printTape ( tape  , 0 , tape_length );

	//now lets execute our turing machine on the input
	//this function is defined in machine.c
	machine( tape  , tape_length );

	//print the result countResult is defined in machine.c
	printf("Answer: %d.\n", countResult( tape , tape_length , nOne , nTwo ) );

	//for the sake of argument lets free the tape
	free(tape);
}