
#ifndef PROJECT_H
#define PROJECT_H

//define our two variables for undifned and 1
#define SYMB_0 " "
#define SYMB_1 "1"

//now lets define our states and symbols
typedef enum { Q0 , Q1 , Q2 , Q3 , Q4 , Q5 , Q6 , Q7 , Q8 , Q9 , Q10  } STATE;
typedef enum { S0 , S1 } SYMBOL;


//define our machine function ( it is different .c file )
//all these functions are in machine.c
extern void machine( SYMBOL * tape  , size_t length );
extern void addToTape ( SYMBOL * tape , int nOne , int nTwo );
extern int countResult ( SYMBOL * tape , size_t length , unsigned int nOne , unsigned int nTwo );
extern void printTape ( SYMBOL * tape , unsigned int pos , size_t length );

#endif